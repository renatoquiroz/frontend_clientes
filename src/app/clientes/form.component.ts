import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ClienteService } from '../services/cliente/cliente.service';
import { Cliente } from './cliente';
import swal from 'sweetalert2';
import { Region } from './region';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html'
})
export class FormComponent implements OnInit {

  public cliente: Cliente = new Cliente();
  public regiones: Region[];
  public titulo: string = "Crear Cliente";

  public errores: string[];

  constructor(
    private clienteService: ClienteService,
    private router: Router,
    private activatedRoute: ActivatedRoute 
  ) { }

  ngOnInit(): void {
    this.cargarCliente()

    this.clienteService.getRegiones().subscribe(regiones => this.regiones = regiones);
  }

  cargarCliente(): void{
    this.activatedRoute.params.subscribe(params =>{
      let id = params['id']
      if(id){
        this.clienteService.getCliente(id).subscribe(
          (cliente) => this.cliente = cliente
        )
      } 
    })
  }

  create(): void{
    this.clienteService.create(this.cliente).subscribe(
      cliente => {
        console.log(this.cliente)
        this.router.navigate(['/clientes'])
        swal.fire('Nuevo Cliente', `Cliente ${cliente.nombre} creado con éxito!`, 'success');
      },
      err => {
        this.errores = err.error.errors as string[];
        console.error('Código del error: '+ err.status);
        console.error(err.error.errors);
      }
      );
  }

  update(): void{
    this.clienteService.update(this.cliente).subscribe(
      cliente => {
        this.router.navigate(['/clientes'])
        swal.fire('Cliente Actualizado', `Cliente ${cliente.nombre} actualizado con éxito!`, 'success')
      },
      err => {
        this.errores = err.error.errors as string[];
        console.error('Código del error: '+ err.status);
        console.error(err.error.errors);
      }
    );
  }

  compararRegion(obj1: Region, obj2: Region): boolean{
    if (obj1 === undefined && obj2 === undefined){
      return true;
    }

    return obj1 == null || obj2 == null ? false: obj1.id === obj2.id;
  }

}
