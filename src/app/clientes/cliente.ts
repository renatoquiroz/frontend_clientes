import { Region } from "./region";

export class Cliente {
    id: number;
    nombre: string;
    fechaNacimiento: Date;
    apellido: string;
    createAt: string;
    email: string;
    foto: string;
    region: Region;
}
