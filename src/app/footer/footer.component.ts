import { Component, OnInit, RendererFactory2 } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  public autor: any = {nombre: 'Renato', apellido: 'Quiroz'};
  
  constructor() { }

  ngOnInit(): void {
  }

}
