import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { Cliente } from 'src/app/clientes/cliente';
import { HttpClient, HttpEvent, HttpHeaders, HttpRequest } from '@angular/common/http';
import { map, catchError} from 'rxjs/operators';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { ClientesComponent } from 'src/app/clientes/clientes.component';
import { formatDate, DatePipe } from '@angular/common';
import { Region } from 'src/app/clientes/region';


@Injectable({
  providedIn: 'root'
})
export class ClienteService {
  private url: string = 'http://localhost:8080/api/clientes';

  private httpHeaders = new HttpHeaders({'Content-Type': 'application/json'})

  constructor(
    private http: HttpClient,
    private router: Router
    ) { }

  getRegiones(): Observable<Region[]>{
    return this.http.get<Region[]>(this.url + '/regiones');
  }

  getClientes(page: number): Observable<any>{
    return this.http.get(this.url + '/page/' + page).pipe(
      map((response: any) => {
        return response
      })
    );
  }

  create(cliente: Cliente): Observable<Cliente>{
    return this.http.post(this.url, cliente, {headers: this.httpHeaders}).pipe(
      map((response: any) => response.cliente as Cliente),
      catchError(e =>{

        if(e.status==400){
          return throwError(e);
        }

        console.error(e.error.mensaje);
        Swal.fire('Error al crear cliente', e.error.mensaje, "error");
        return throwError(e);
      })
    );
  }

  getCliente(id): Observable<Cliente>{
    return this.http.get<Cliente>(`${this.url}/${id}`).pipe(
      catchError(e => {
        this.router.navigate(['/clientes'])
        console.error(e.error.mensaje);
        Swal.fire('Error, cliente no existe', e.error.mensaje, 'error');
        return throwError(e);
      })
    );
  }

  update(cliente: Cliente): Observable<Cliente>{
    return this.http.put<Cliente>(`${this.url}/${cliente.id}`, cliente, {headers: this.httpHeaders}).pipe(
      map((response: any) => response.cliente as Cliente),
      catchError(e =>{

        if(e.status==400){
          return throwError(e);
        }

        console.error(e.error.mensaje);
        Swal.fire('Error al intentar editar cliente', e.error.mensaje, "error");
        return throwError(e);
      })
    );
  }

  delete(id: number): Observable<Cliente>{
    return this.http.delete<Cliente>(`${this.url}/${id}`, {headers: this.httpHeaders}).pipe(
      map((response: any) => response.cliente as Cliente),
      catchError(e =>{
        console.error(e.error.mensaje);
        Swal.fire('Error al intentar eliminar cliente', e.error.mensaje, "error");
        return throwError(e);
      })
    );
  }

  subirFoto(archivo: File, id): Observable<HttpEvent<{}>>{
    let formData = new FormData();
    formData.append("archivo", archivo);
    formData.append("id", id);

    const req = new HttpRequest('POST',`${this.url}/upload`, formData,{
      reportProgress: true
    });

    return this.http.request(req);
  }

}
